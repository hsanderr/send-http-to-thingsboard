// Require process to get access token
const process = require('process');

// Require axios to perform the POST request
const axios = require('axios').default;

// Set header properly
const config = {
    headers: {
        'Content-Type': 'application/json'
    }
}

// Set access token for ThingsBoard device
const accessToken = process.argv[2];

// Set URL to perform the request to
const url = `https://demo.thingsboard.io/api/v1/${accessToken}/telemetry`;

/* Function to send random temperature (between 20 and 30 celsius degreees) 
and relative humidity (between 50 and 60%) */
const sendTelemetry = () => {
    axios.post(url, {
        temperature: Math.floor(Math.random() * 10) + 20,
        relativeHumidity: Math.floor(Math.random() * 10) + 50
    }, config)
        .then(function (response) {
            console.log('Sucesso:\n', response);
        })
        .catch(function (error) {
            console.log('Erro:\n', error);
        });
}

// Sends telemetry every 2000 miliseconds
setInterval(sendTelemetry, 2000);
