# Send Random to ThingsBoard via HTTP

This project sends random data to ThingsBoard Cloud every 2 seconds. The data consists in a temperature between 20 and 30 (celsius degrees) and a relative humidity between 50 and 60 (%).

## Pre-requisites

- Having Node.js installed. [Download from official website.](https://nodejs.org/en/download/)

## Instructions

1. Clone this repository: `git clone https://gitlab.com/isensi/send-random-to-thingsboard-via-http`

2. Install the dependencies. Inside the repository: `npm install`

3. Run the code with the access token as a process argument (replace ACCESSTOKEN with the device access token): `node main.js ACCESSTOKEN`



